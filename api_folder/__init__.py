from flask import Flask
import os
from os.path import join, dirname
from dotenv import load_dotenv
from flask_migrate import Migrate
from flask_restful import Api

from api_folder.error_pages.handlers import error_pages
from api_folder.extensions import db
from api_folder.user.user_resource import UserResource, UserNameResource

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

app = Flask(__name__)

app.config[
    'SQLALCHEMY_DATABASE_URI'] = f"postgresql://{os.environ.get('USER_NAME')}:" \
                                 f"{os.environ.get('USER_PASSWORD')}" \
                                 f"@{os.environ.get('HOST')}:" \
                                 f"{os.environ.get('DATABASE_PORT')}/" \
                                 f"{os.environ.get('DATABASE_NAME')}"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)
Migrate(app, db)

api = Api(app, catch_all_404s=True)
api.add_resource(UserResource, '/users')
api.add_resource(UserNameResource, '/users/<name>')

app.register_blueprint(error_pages)
