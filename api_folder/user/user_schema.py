from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from api_folder.user.user_model import User


class UserSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = User
        load_instance = True
        load_only = ("user_id", "password")
