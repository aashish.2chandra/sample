from flask_bcrypt import Bcrypt
from flask_restful import Resource, abort
from flask import request
from api_folder.extensions import db
from api_folder.user.user_model import User
from api_folder.user.user_schema import UserSchema


user_schema = UserSchema()
users_schema = UserSchema(many=True)


class UserResource(Resource):

    def get(self):
        users = User.query.all()
        return users_schema.dump(users)

    def post(self):
        bcrypt = Bcrypt()
        name = request.json['username']

        if User.query.filter_by(username=name).first():
            abort(403, pick_another_name="user already exists")

        data = {"username": name,
                "password": bcrypt.generate_password_hash(request.json['password']),
                "role": request.json['role'],
                "address": request.json['address'],
                "email": request.json['email']}

        user_data = user_schema.load(data, session=db.session)
        db.session.add(user_data)
        db.session.commit()

        return user_schema.dump(user_data)


class UserNameResource(Resource):

    def get(self, name):
        user = User.query.filter_by(username=name).first()
        if not user:
            abort(404, wrong_user_name="no such user exists")
        return user_schema.dump(user)

    def delete(self, name):
        user = User.query.filter_by(username=name).first()
        if not user:
            abort(404, wrong_user_name="no such user exists")
        db.session.delete(user)
        db.session.commit()
        return user_schema.dump(user)
