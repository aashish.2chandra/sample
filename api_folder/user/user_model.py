from api_folder.extensions import db
from sqlalchemy.dialects.postgresql import UUID
import uuid


class User(db.Model):

    __tablename__ = 'users'

    user_id = db.Column((UUID(as_uuid=True)), primary_key=True, default=uuid.uuid4, unique=True)
    username = db.Column(db.String(50), index=True, unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)
    role = db.Column(db.String(50))
    address = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(50))
    verify = db.Column(db.Boolean(), default=True)

    def __init__(self, username, password, role, address, email):
        self.username = username
        self.password = password
        self.role = role
        self.address = address
        self.email = email
