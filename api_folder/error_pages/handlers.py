from flask import Blueprint
from flask import json
from werkzeug.exceptions import HTTPException

error_pages = Blueprint('error_pages', __name__)


@error_pages.errorhandler(404)
def error_404(error):
    return {"error": str(error)}, 404


@error_pages.errorhandler(403)
def error_403(error):
    return {"error": str(error)}, 403


@error_pages.errorhandler(HTTPException)
def handle_exception(e):
    response = e.get_response()
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response


@error_pages.errorhandler(ValueError)
def error_500(error):
    return {"error": str(error)}
